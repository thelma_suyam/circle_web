<?php 

	$page_title = "Event Detail";
	include_once('includes/header.php'); 

?>

<main class="event_detail event_detail_photo">
	<div class="container02 event_detail__container">
		<div class="main_detail">
			<section class="event_main_detail">
				<div class="event_cover" style="background-image: url('./assets/img/event/pic_01.png')">
					<div class="event_cat">デザイン・アート</div>
				</div>
				<h1 class="event_title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！- </h1>
				<div class="event_info">
					<span class="event_info__date">2017/11/1(水) ～ 2018/1/8(月)</span>
					<span class="event_info__location">森アーツセンターギャラリー</span>
					<span class="event_info__currency">3,000円 ～ 5,000円</span>
					<span class="event_info__users">無制限</span>
				</div>
				<div class="event_map_container sp">
					<div class="event_map">
						<div class="mapouter"><div class="gmap_canvas"><iframe width="640" height="120" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div></div>
					</div>
					<div class="event_address">東京都豊島区雑司が谷３丁目15-20</div>
				</div>
			</section>
			<section>
				<div class="previous">
					アルバム
				</div>
				<div class="preview_gallery">
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_01.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_02.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_03.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_04.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_05.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_06.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_07.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_08.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_09.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_10.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_11.png')">
						<div class="delete_img"></div>
					</div>
					<div class="preview_img" style="background-image: url('./assets/img/event/pic_01.png')">
						<div class="delete_img"></div>
					</div>
				</div>
				<div class="pagination">
					<div class="pagination__prev">前のページ</div>
					<div class="pagination__current">02/06</div>
					<div class="pagination__next">次のページ</div>
				</div>
			</section>
		</div>
		<div class="sub_detail pc">
			<div class="event_owner">
				<div class="event_owner__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
				<div class="event_owner__details">
					<div class="event_owner__username">Thelma Pogi</div>
					<div class="event_owner__follow">フォローする</div>
					<div class="event_owner__mail"></div>
				</div>
			</div>
			<div class="event_map">
				<div class="mapouter"><div class="gmap_canvas"><iframe width="640" height="640" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div></div>
			</div>
			<div class="event_address">東京都豊島区雑司が谷３丁目15-20</div>
			<div class="event_social">
				<ul>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_twitter.png">
						</a>
						twitter
					</li>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_fb.png">
						</a>
						facebook
					</li>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_googleplus.png">
						</a>
						google+
					</li>
				</ul>
			</div>
			<button class="btn btn--primary_invert">参加する</button>
			<div class="event_interest"><span>興味のあるイベント</span></div>
		</div>
	</div>
	<div class="sub_detail sp container02">
		<button class="btn btn--primary_invert">参加する</button>
		<div class="sub_detail_clickables">
			<div class="event_interest"><span>興味のあるイベント</span></div>
			<div class="event_social">
				<a href="" class="twitter">
					<img src="./assets/img/common/ico_twitter_sm.png">
				</a>
				<a href="" class="fb">
					<img src="./assets/img/common/ico_fb_sm.png">
				</a>
				<a href="" class="google">
					<img src="./assets/img/common/ico_googleplus_sm.png">
				</a>
			</div>
		</div>
	</div>
		
</main>

<?php include_once('includes/footer.php'); ?>