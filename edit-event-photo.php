<?php 

	$page_title = "Edit Event Photo";
	include_once('includes/header.php'); 

?>

<main class="create_edit edit_photo">
	<div class="container02">
		<header class="form_header">
			<h1>イベント編集する</h1>
			<p class="pc">ダミーです。あなたが興味あるCIRCLEイベントを選択してください。</p>
			<p class="sp">あなたが興味あるCIRCLEイベントを選択してください。</p>
		</header>
		<div class="edit_photo__header">
			<div class="previous">
				フォロー <span>(52人)</span>
			</div>
			<a href="" class="txt--red">削除</a>
		</div>
	</div>
	<div class="container02">
		<form class="default_form">
			<div class="preview_gallery">
				<label class="upload_img"><input type="file" multiple name="" id="gallery-photo-add"></label>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_01.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_02.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_03.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_04.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_05.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_06.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_07.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_08.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_09.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_10.png')">
					<div class="delete_img"></div>
				</div>
				<div class="preview_img" style="background-image: url('./assets/img/event/pic_11.png')">
					<div class="delete_img"></div>
				</div>
			</div>
			<div class="form_input submit_btn">
				<div class="container">
					<button class="btn btn--white"><span>キャンセル</span></button>
					<input class="btn btn--primary_invert" type="submit" value="登録する">
				</div>
			</div>
		</form>
		<div class="pagination">
			<div class="pagination__prev">前のページ</div>
			<div class="pagination__current">02/06</div>
			<div class="pagination__next">次のページ</div>
		</div>
	</div>
</main>

<?php include_once('includes/footer.php'); ?>