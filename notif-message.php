<?php 
	
	$page_title = "Notifications";
	include_once('includes/header.php');

?>

<main class="notif_messages">
	<div class="container notif_messages__container">
		<ul class="notif_tab">
			<li>
				<a data-panel="notifications_page" class="active"><span>お知らせ</span></a>
			</li>
			<li>
				<a data-panel="messages_page"><span>受信トレイ</span></a>
			</li>
		</ul>
		<div class="notif_panel visible" panel-id="notifications_page">
			<div class="notif_list">
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/don.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Don Flores</p>
						<p class="notif_item__msg"><span>Live Android</span>を参加しました。</p>
						<p class="notif_item__date">今</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete01"></div>
						<div class="btn_container" data-id="delete01">
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/sylvia.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Sylvia Boyd</p>
						<p class="notif_item__msg"><span>ユッキー </span>をフォローしました。</p>
						<p class="notif_item__date">2018/04/13 12:12</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete02"></div>
						<div class="btn_container" data-id="delete02">
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/clifford.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Clifford Carr</p>
						<p class="notif_item__msg"><span>Live Android</span>を参加しました。</p>
						<p class="notif_item__date">2018/04/11 12:12</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete03"></div>
						<div class="btn_container" data-id="delete03">
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/michael.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Michael</p>
						<p class="notif_item__msg"><span>ユッキー</span>をフォローしました。</p>
						<p class="notif_item__date">3時間前</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete04"></div>
						<div class="btn_container" data-id="delete04">
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/jlo.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">jlo</p>
						<p class="notif_item__msg"><span>あなたのLive iOS</span>のイベントに参加者jloさんが追加されました。</p>
						<p class="notif_item__date">2018/04/12</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete05"></div>
						<div class="btn_container" data-id="delete05">
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="notif_panel" panel-id="messages_page" hidden>
			<div class="notif_list">
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Thelma Pogi</p>
						<p class="notif_item__msg">雑貨屋、カフェ、レストラン、目黒川などなど素敵なスポットがたくさんあるのでまあなんとかなります。お金がなくてもお金があっても楽しめるエリアって素晴らしい。</p>
						<p class="notif_item__date">2018/09/12</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete06"></div>
						<div class="btn_container" data-id="delete06">
							<button class="btn--white" data-panel="reply_page">返信する</button>
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Rosetta</p>
						<p class="notif_item__msg">ユッキーをフォローしました。</p>
						<p class="notif_item__date">3時間前</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete07"></div>
						<div class="btn_container" data-id="delete07">
							<button class="btn--white" data-panel="reply_page">返信する</button>
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
				<div class="notif_item">
					<div class="notif_item__img" style="background-image: url('./assets/img/user/eula.png')"></div>
					<a class="notif_item__details">
						<p class="notif_item__name">Eula</p>
						<p class="notif_item__msg">のイベントに参加者jloさんが追加されました。</p>
						<p class="notif_item__date">2018/04/12</p>
					</a>
					<div class="delete_option">
						<div class="hide-show" data-target="delete08"></div>
						<div class="btn_container" data-id="delete08">
							<button class="btn--white" data-panel="reply_page">返信する</button>
							<button class="btn--white">削除する</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="notif_panel" panel-id="reply_page" hidden>
			<div class="previous">
				Don Flores さんとの会話
			</div>
			<div class="convo">
				<div class="msg">
					<div class="msg__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">外国人には日本のラーメンはものすごい人気。その中でも豚骨でかつニューヨークでも超人気店ってことで一風堂の知名度が高いようです。滞在期間中連れて一度は連れて行ってあげましょう。</p>
					</div>
				</div>
				<div class="msg msg--reply">
					<div class="msg__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">ありがとうございます！！！</p>
					</div>
				</div>
				<div class="msg">
					<div class="msg__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
					</div>
				</div>
				<div class="msg msg--reply">
					<div class="msg__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">ユッキーをフォローしました。</p>
					</div>
				</div>
				<div class="msg">
					<div class="msg__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">居酒屋メニューなので和系ならなんでも出てきます。外国の和食レストランみたい。でも有名なので味のわりには高いなとの印象です。キルビル的観光地として覚悟してご利用下さい。</p>
					</div>
				</div>
				<div class="msg msg--reply">
					<div class="msg__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">居酒屋メニューなので和系ならなんでも出てきます。外国の和食レストランみたい。でも有名なので味のわりには高いなとの印象です。キルビル的観光地として覚悟してご利用下さい。</p>
					</div>
				</div>
				<div class="msg">
					<div class="msg__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">雑貨屋、カフェ、レストラン、目黒川などなど素敵なスポットがたくさんあるのでまあなんとかなります。お金がなくてもお金があっても楽しめるエリアって素晴らしい。</p>
					</div>
				</div>
				<div class="msg msg--reply">
					<div class="msg__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
					<div class="msg__content">
						<p class="msg__txt">ユッキーをフォローしました。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="reply" panel-id="reply_page" hidden>
		<div class="container">
			<form class="">
				<div class="form_input">
					<textarea></textarea>
				</div>
				<div class="form_input reply_btn">
					<label class="custom_checkbox">
						利用規約を同意する
					  	<input type="checkbox" checked="checked">
					  	<span class="checkmark"></span>
					</label>
					<input type="submit" name="" value="送信する" class="btn btn--primary">
				</div>
			</form>
		</div>
	</div>
</main>

<?php include_once('includes/footer.php'); ?>