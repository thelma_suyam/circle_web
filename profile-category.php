<?php 
	
	$page_title = "Profile Categories";
	include_once('includes/header.php');
	include_once('includes/profile_header.php');

?>

<nav class="profile_nav container">
	<ul>
		<li>
			<a href="profile-schedule">参加予定<br class="sp">イベント</a>
		</li>
		<li>
			<a href="">参加した<br class="sp">イベント</a>
		</li>
		<li>
			<a href="profile-create">作成<br class="sp">イベント</a>
		</li>
		<li>
			<a href="">興味のある<br class="sp">イベント</a>
		</li>
		<li>
			<a href="profile-category" class="active">カテゴリー</a>
		</li>
	</ul>
</nav>

<main class="profile">
	<div class="container">
		<!-- category -->
		<div class="profile_category profile_panel" id="profile_category">
			<section class="category">
				<div class="category__items_container">
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_01.jpg');">
						</div>
						<p class="category__item_title">
							音  楽
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_02.jpg');">
						</div>
						<p class="category__item_title">
							デザイン・アート
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_03.jpg');">
						</div>
						<p class="category__item_title">
							劇・芝居
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_04.jpg');">
						</div>
						<p class="category__item_title">
							フード
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_05.jpg');">
						</div>
						<p class="category__item_title">
							ペット
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_06.jpg');">
						</div>
						<p class="category__item_title">
							子供
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
				</div>
			</section>
			<section class="category">
				<h2 class="profile_category__section_title">カテゴリーを追加</h2>
				<div class="category__items_container">
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_08.jpg');">
						</div>
						<p class="category__item_title">
							Web
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_09.jpg');">
						</div>
						<p class="category__item_title">
							プログラミング
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_10.jpg');">
						</div>
						<p class="category__item_title">
							ゲーム
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_11.jpg');">
						</div>
						<p class="category__item_title">
							芸能
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_12.jpg');">
						</div>
						<p class="category__item_title">
							アウトドア
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_13.jpg');">
						</div>
						<p class="category__item_title">
							旅行
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_14.jpg');">
						</div>
						<p class="category__item_title">
							季節限定
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_15.jpg');">
						</div>
						<p class="category__item_title">
							フォト
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_16.jpg');">
						</div>
						<p class="category__item_title">
							展示会
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_17.jpg');">
						</div>
						<p class="category__item_title">
							文化
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_18.jpg');">
						</div>
						<p class="category__item_title">
							企業交流会
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_19.jpg');">
						</div>
						<p class="category__item_title">
							セミナー
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_20.jpg');">
						</div>
						<p class="category__item_title">
							ワークショップ
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
					<div class="category__item">
						<div class="category__item_img" style="background-image: url('./assets/img/category/pic_21.jpg');">
						</div>
						<p class="category__item_title">
							サークル
						</p>
						<p class="txt--red category__item_add">
							追加する
						</p>
					</div>
				</div>
			</section>
		</div><!-- category -->
	</div>
</main>

<?php include_once('includes/footer.php') ?>