<?php 
	$page_title = "Events";
	include_once('includes/header.php') 
?>


<main class="event">
	<header>
		<div class="search__container">
			<input type="search">
			<button class="locate"><span>豊島区</span></button>
			<input type="text" id="datepicker">
		</div> <!-- .search__container -->
	</header>
	<section class="section__01">
		<div class="container">
			<div class="category__container">
				<ul class="category__list">
					<li class="active"><a href="#">全部・791318</a></li>
					<li><a href="#">音楽・300</a></li>
					<li><a href="#">デザインアート・1203</a></li>
					<li><a href="#">劇・芝居・1000</a></li>
					<li><a href="#">フード・600</a></li>
					<li><a href="#">ペット・634</a></li>
					<li><a href="#">子供・1432</a></li>	
					<li><a href="#">インテリア・1203</a></li>
					<li><a href="#">Web・2203</a></li>
					<li><a href="#">プログラミング・1203</a></li>
					<li><a href="#">ゲーム・1203</a></li>
					<li><a href="#">芸能・1203</a></li>
					<li><a href="#">アウトドア・1203</a></li>	
					<li><a href="#">旅行・1203</a></li>
					<li><a href="#">季節限定・373</a></li>	
					<li><a href="#">フォト・1203</a></li>
					<li><a href="#">展示会・990</a></li>
					<li><a href="#">文化・1455</a></li>
					<li><a href="#">企業交流会・2640</a></li>
					<li><a href="#">セミナー・2493</a></li>
					<li><a href="#">ワークショップ・1203</a></li>	
					<li><a href="#">サークル・49</a></li>	
				</ul>
				<a href="javascript:void(0);" class="catnav__btn"></a>
			</div>
		</div>
	</section> <!-- .section__01 -->

	<section class="section__02">
		<div class="container">
			<div class="eventcases__category">
				<div class="date"><span>2018/04/12</span>(木)</div>
				<div class="item">イベント <span>28件</span></div>
			</div> <!-- .eventcases__category -->
			<div class="event_list">
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_01.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_02.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">HOT！ほっとスイーツ2018</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_03.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">レアンドロ・エルリッヒ展：見ることのリアル</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_04.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">SNOW AQUARIUM by NAKED <br class="pc">ーCRYSTAL MAGICー</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_05.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_06.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_07.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_08.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_09.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_10.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_11.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_12.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
			</div><!-- .event_list -->
		</div> <!-- .inner__container -->
	</section> <!-- .section__02 -->

</main>

<?php include_once('includes/footer.php') ?>