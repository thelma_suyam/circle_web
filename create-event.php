<?php 
	
	$page_title = "Create Event";
	include_once('includes/header.php') ?>

<main class="create_edit">
	<form>
		<div class="container">
			<header class="form_header">
				<h1>イベント作成</h1>
				<p class="pc">ダミーです。あなたが興味あるCIRCLEイベントを選択してください。</p>
				<p class="sp">あなたが興味あるCIRCLEイベントを選択してください。</p>
			</header>
		</div>
		<div class="create_edit__form default_form">
			<div class="container">
				<div class="form_input">
					<label>タイトル</label>
					<input type="" class="full-width" name="" placeholder="タイトル">
				</div>
				<div class="form_input">
					<label>カテゴリー</label>
					<div class="custom_select">
					  <select>
					    <option value="0" hidden>デザイン・アート</option>
					    <option value="1">Option A</option>
					    <option value="2">Option B</option>
					    <option value="3">Option C</option>
					    <option value="4">Option D</option>
					  </select>
					</div>
				</div>
				<div class="form_input center_align">
					<label>日時</label>
					<input type="text" id="duration_from" class="datepicker" placeholder="2018/04/11 00:00">
					<div class="to"></div>
					<input type="text" id="duration_til" class="datepicker" placeholder="2018/04/11 00:00">
				</div>
				<div class="form_input">
					<label>場所</label>
					<input type="" class="full-width" name="" placeholder="東京都港区六本木6-10-1 六本木ヒルズ森タワー52階">
				</div>
				<div class="form_input">
					<label>参加人数</label>
					<input type="" name="" placeholder="参加人数">
				</div>
				<div class="form_input">
					<label>参加費</label>
					<input type="" name="" placeholder="3,000円〜５,000円">
				</div>
				<div class="form_input">
					<label>イベント写真</label>
					<div class="preview_gallery">
						<label class="upload_img"><input type="file" multiple name="" id="gallery-photo-add"></label>
					</div>
				</div>
				<div class="form_input">
					<label>イベント紹介</label>
					<textarea placeholder="" class="full-width">イベント紹介</textarea>
				</div>
				<div class="form_input">
					<label>公開設定</label>
					<label class="custom_radio">
						一般公開
						<input type="radio" name="rad">
						<span class="checkmark"></span>
					</label>
					<label class="custom_radio">
						友達のみ
						<input type="radio" name="rad">
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="form_input submit_btn">
				<div class="container">
					<button class="btn btn--primary_invert"><span>登録する</span></button>
				</div>
			</div>
		</div>
	</form>
</main>

<?php include_once('includes/footer.php') ?>