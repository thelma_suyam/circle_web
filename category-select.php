<?php 
	
	$page_title = "Category Select";
	include_once('includes/header.php') ?>

<header class="page_header container">
	<div class="page_header__col">
		<h1>イベントカテゴリー</h1>
		<p>あなたが興味あるCIRCLEイベントを選択してください。</p>
	</div>
	<div class="page_header__col pc">
		<button class="btn btn--primary_invert"><span>登録する</span></button>
	</div>
</header>

<main class="category category_select">
	<div class="container">
		<div class="category__items_container">
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_01.jpg');">
				</div>
				<p class="category__item_title">
					音  楽
				</p>
				<p class="category__item_count">
					<span>300</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_02.jpg');">
				</div>
				<p class="category__item_title">
					デザイン・アート
				</p>
				<p class="category__item_count">
					<span>1203</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_03.jpg');">
				</div>
				<p class="category__item_title">
					劇・芝居
				</p>
				<p class="category__item_count">
					<span>1000</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_04.jpg');">
				</div>
				<p class="category__item_title">
					フード
				</p>
				<p class="category__item_count">
					<span>600</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_05.jpg');">
				</div>
				<p class="category__item_title">
					ペット
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_06.jpg');">
				</div>
				<p class="category__item_title">
					子供
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_07.jpg');">
				</div>
				<p class="category__item_title">
					インテリア
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_08.jpg');">
				</div>
				<p class="category__item_title">
					Web
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_09.jpg');">
				</div>
				<p class="category__item_title">
					プログラミング
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_10.jpg');">
				</div>
				<p class="category__item_title">
					ゲーム
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_11.jpg');">
				</div>
				<p class="category__item_title">
					芸能
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_12.jpg');">
				</div>
				<p class="category__item_title">
					アウトドア
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_13.jpg');">
				</div>
				<p class="category__item_title">
					旅行
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_14.jpg');">
				</div>
				<p class="category__item_title">
					季節限定
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item selected">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_15.jpg');">
				</div>
				<p class="category__item_title">
					フォト
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_16.jpg');">
				</div>
				<p class="category__item_title">
					展示会
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_17.jpg');">
				</div>
				<p class="category__item_title">
					文化
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_18.jpg');">
				</div>
				<p class="category__item_title">
					企業交流会
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_19.jpg');">
				</div>
				<p class="category__item_title">
					セミナー
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_20.jpg');">
				</div>
				<p class="category__item_title">
					ワークショップ
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
			<div class="category__item">
				<div class="category__item_img" style="background-image: url('./assets/img/category/pic_21.jpg');">
				</div>
				<p class="category__item_title">
					サークル
				</p>
				<p class="category__item_count">
					<span>634</span>件
				</p>
			</div>
		</div>
	</div>
	<div class="sp button_container">
		<button class="btn btn--primary_invert"><span>登録する</span></button>
	</div>
</main>

<?php include_once('includes/footer.php') ?>