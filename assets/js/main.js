// scroll effect on anchor tag to id
function scroll() {
	$('a[href^="#"]').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('html, body').stop().animate({ scrollTop: position }, 500, 'swing');
		return false;
	});
}

function modalOpen() {
	$('.ico__close').on('click', function () {
		$('.modal').fadeOut();
		$('body').removeClass('modal_open');
	});

	$('.btn--login').on('click', function () {
		$('#login__modal').fadeIn();
		$('body').addClass('modal_open');
	});

	$('.btn--register').on('click', function () {
		$('#register__modal').fadeIn();
		$('body').addClass('modal_open');
	});
}

function view() {
	$('.catnav__btn').on('click', function () {
		$(this).prev('.category__list').toggleClass('js--category__list');
	});
}

function fixLayout() {
	$('.img__item img').matchHeight();
	$('.feature__details h3').matchHeight();

	// pogi person
	$('.event_item').matchHeight();
	$('.event_list__title').matchHeight();
}

function multipleImgPreview() {
	// Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $($.parseHTML('<div class="preview_img">')).css({'background-image' : 'url(' + event.target.result + ')'}).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.preview_gallery');
    });
}

function initDatepicker() {
	$('.datepicker').each(function() {
		$(this).datepicker({
			dateFormat: 'yy-mm-dd hh:mm',
			changeMonth: false
		});
		$('.ui-datepicker-title').html('hello');
	})
}

function initSlider() {
	$('.preview_slider').slick({
		slidesToShow: 3,
		arrows: true,
		prevArrow: $('.preview_slider__prev'),
		nextArrow: $('.preview_slider__next'),
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2
			}
		}]
	})

	$('.photo_preview_slider').slick({
		slidesToShow: 3,
		arrows: true,
		prevArrow: $('.photo_preview_slider__prev'),
		nextArrow: $('.photo_preview_slider__next'),
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3
			}
		}]
	})

	$('.event_slider').slick({
		arrows: true,
		variableWidth: true,
		prevArrow: $('.event_slider__prev'),
		nextArrow: $('.event_slider__next'),
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1
			}
		}]
	})

}

function customSelect() {
	var x, i, j, selElmnt, a, b, c;
	/*look for any elements with the class "custom_select":*/
	x = document.getElementsByClassName("custom_select");
	for (i = 0; i < x.length; i++) {
	  selElmnt = x[i].getElementsByTagName("select")[0];
	  /*for each element, create a new DIV that will act as the selected item:*/
	  a = document.createElement("DIV");
	  a.setAttribute("class", "select-selected");
	  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	  x[i].appendChild(a);
	  /*for each element, create a new DIV that will contain the option list:*/
	  b = document.createElement("DIV");
	  b.setAttribute("class", "select-items select-hide");
	  for (j = 0; j < selElmnt.length; j++) {
	    /*for each option in the original select element,
	    create a new DIV that will act as an option item:*/
	    c = document.createElement("DIV");
	    c.innerHTML = selElmnt.options[j].innerHTML;
	    c.addEventListener("click", function(e) {
	        /*when an item is clicked, update the original select box,
	        and the selected item:*/
	        var y, i, k, s, h;
	        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
	        h = this.parentNode.previousSibling;
	        for (i = 0; i < s.length; i++) {
	          if (s.options[i].innerHTML == this.innerHTML) {
	            s.selectedIndex = i;
	            h.innerHTML = this.innerHTML;
	            y = this.parentNode.getElementsByClassName("same-as-selected");
	            for (k = 0; k < y.length; k++) {
	              y[k].removeAttribute("class");
	            }
	            this.setAttribute("class", "same-as-selected");
	            break;
	          }
	        }
	        h.click();
	    });
	    b.appendChild(c);
	  }
	  x[i].appendChild(b);
	  a.addEventListener("click", function(e) {
	      /*when the select box is clicked, close any other select boxes,
	      and open/close the current select box:*/
	      e.stopPropagation();
	      closeAllSelect(this);
	      this.nextSibling.classList.toggle("select-hide");
	      this.classList.toggle("select-arrow-active");
	    });
	}
	function closeAllSelect(elmnt) {
	  /*a function that will close all select boxes in the document,
	  except the current select box:*/
	  var x, y, i, arrNo = [];
	  x = document.getElementsByClassName("select-items");
	  y = document.getElementsByClassName("select-selected");
	  for (i = 0; i < y.length; i++) {
	    if (elmnt == y[i]) {
	      arrNo.push(i)
	    } else {
	      y[i].classList.remove("select-arrow-active");
	    }
	  }
	  for (i = 0; i < x.length; i++) {
	    if (arrNo.indexOf(i)) {
	      x[i].classList.add("select-hide");
	    }
	  }
	}
	/*if the user clicks anywhere outside the select box,
	then close all select boxes:*/
	document.addEventListener("click", closeAllSelect);

}

function profileNav() {
	$('.profile_nav li a').on('click', function() {
		var target = $(this). attr('data-target');

		$('.profile_panel').hide();

		$('#' + target).show();
	})
}

function openTarget() {
	$('*[data-target]').on('click', function() {
		var target = $(this).attr('data-target');
		
		$(this).toggleClass('open');

		$('*[data-id="'+target+'"]').toggleClass('visible');
	})

	// open header panels
	$('*[header-panel]').on('click', function() {
		var target = $(this).attr('header-panel');

		$('*[header-panel]').removeClass('active');
		$(this).addClass('active');

		$('*[header-panel-id]').removeClass('visible');
		$('*[header-panel-id="'+target+'"]').toggleClass('visible');
	})

	// open panels
	$('*[data-panel]').on('click', function() {
		var target = $(this).attr('data-panel');

		$('*[data-panel]').removeClass('active');
		$(this).addClass('active');

		$('*[panel-id]').removeClass('visible');
		$('*[panel-id="'+target+'"]').toggleClass('visible');
	})
}

function onModalSliderOpen() {
	// photo index indicator
	var $gallery = $('.modal_slider');
	var slideCount = null;

	$( document ).ready(function() {
		$gallery.slick({
			slidesToShow: 1,
			dots: true,
			arrows: true,
			prevArrow: $('.modal_slider__prev'),
			nextArrow: $('.modal_slider__next'),
		})
	})

	$('.photo_preview_slider').on('init', function(event, slick){
	  slideCount = slick.slideCount;
	  console.log(slick.slideCount);
	  setSlideCount();
	  setCurrentSlideNumber(slick.currentSlide);
	});
	  console.log('hello' + slideCount);

	$gallery.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  setCurrentSlideNumber(nextSlide);
	});

	function setSlideCount() {
	  var $el = $('.modal__image_count').find('.total');
	  $el.text(slideCount);
	}

	function setCurrentSlideNumber(currentSlide) {
	  var $el = $('.modal__image_count').find('.current');
	  $el.text(currentSlide + 1);
	}

	// go to slide on modal open
	$('.photo_slider .slick-slide').on('click', function() {
		var currentIndex = $(this).data("slick-index");
		$('.modal_slider').slick('slickGoTo', currentIndex);
	})
}

function closeOpenModal() {

	$('*[modal-target]').on('click', function(e) {
		e.preventDefault();
		var target = $(this).attr('modal-target');

		$(this).closest('body').addClass('modal_open');

		if (target !== 'modal--slider') {
			$('*[modal-id="'+target+'"]').fadeIn(500, function() {
				$(this).toggleClass('visible');
			});
		} else {
			
			$('*[modal-id="'+target+'"]').fadeIn(500, function() {
				$(this).toggleClass('visible');
			});
		}
	})

	$('.modal__close').on('click', function() {
		$(this).closest('.modal').fadeOut(500, function() {
			$(this).removeClass('visible');
			$(this).closest('body').removeClass('modal_open');
		});
	})
}

function likeEvent() {
	$('.event_interest').on('click', function() {
		$(this).toggleClass('liked');
	})
}


$(function(){
	scroll();
	fixLayout();
	modalOpen();
	view();
	customSelect();
	initDatepicker();
	initSlider();
	multipleImgPreview();
	profileNav();
	openTarget();
	closeOpenModal();
	likeEvent();
	$( "#datepicker" ).datepicker();
});

onModalSliderOpen();

$(window).on('load resize', function(){
}); // resize