<?php 
	
	$page_title = "Followers";
	include_once('includes/header.php');
	include_once('includes/profile_header.php');

?>

<main class="profile">
	<div class="container">
		<!-- follower -->
		<div class="follower profile_panel" id="follower">
			<div class="previous">
				フォロー <span>(52人)</span>
			</div>
			<div class="user_list">
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/eula.png')">
					</div>
					<div class="user_item__username">Eula Houston</div>
					<div class="user_item__bio">音楽、ペット、デザインアート、アウトドア、芸能、...</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/hattie.png')">
					</div>
					<div class="user_item__username">Hattie Sullivan</div>
					<div class="user_item__bio">音楽、ペット、旅行、アウトドア、芸能、プログラミング</div>
					<div class="user_item__follow_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/rosetta.png')">
					</div>
					<div class="user_item__username">Rosetta Lloyd</div>
					<div class="user_item__bio">音楽、ペット、劇・芝居、インテリア、アウトドア</div>
					<div class="user_item__follow_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/mae.png')">
					</div>
					<div class="user_item__username">Mae Fleming</div>
					<div class="user_item__bio">フード、音楽、ペット、デザインアート、アウトドア、...</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/nora.png')">
					</div>
					<div class="user_item__username">Nora Wade</div>
					<div class="user_item__bio">音楽、Web、デザインアート、アウトドア、フォト</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/eula.png')">
					</div>
					<div class="user_item__username">Eula Houston</div>
					<div class="user_item__bio">音楽、ペット、デザインアート、アウトドア、芸能、...</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/hattie.png')">
					</div>
					<div class="user_item__username">Hattie Sullivan</div>
					<div class="user_item__bio">音楽、ペット、旅行、アウトドア、芸能、プログラミング</div>
					<div class="user_item__follow_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/rosetta.png')">
					</div>
					<div class="user_item__username">Rosetta Lloyd</div>
					<div class="user_item__bio">音楽、ペット、劇・芝居、インテリア、アウトドア</div>
					<div class="user_item__follow_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/mae.png')">
					</div>
					<div class="user_item__username">Mae Fleming</div>
					<div class="user_item__bio">フード、音楽、ペット、デザインアート、アウトドア、...</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
				<div class="user_item">
					<div class="user_item__img" style="background-image: url('./assets/img/user/nora.png')">
					</div>
					<div class="user_item__username">Nora Wade</div>
					<div class="user_item__bio">音楽、Web、デザインアート、アウトドア、フォト</div>
					<div class="user_item__following_btn"><span>フォロー中</span></div>
				</div><!-- /user_item -->
			</div>
		</div><!-- follower -->

	</div>
</main>

<?php include_once('includes/footer.php') ?>