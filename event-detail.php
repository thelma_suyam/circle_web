<?php 

	$page_title = "Event Detail";
	include_once('includes/header.php'); 

?>

<div class="modal modal--slider" modal-id="modal--slider">
	<div class="modal__flexcontainer">
		<div class="modal_slider_container">
			<div class="modal__close"></div>
			<div class="modal__image_count"><span class="current">02</span>/<span class="total">100</span></div>
			<div class="modal_slider">
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_05.png');"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_06.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_07.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_08.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_01.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_02.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_03.png')"></div>
				</div>
				<div class="modal_item">
					<div class="modal_item__img" style="background-image: url('./assets/img/event/pic_04.png')"></div>
				</div>
			</div>
			<div class="modal_slider__prev"></div>
			<div class="modal_slider__next"></div>
		</div>
	</div>
</div>

<div class="modal modal--comments" modal-id="modal--comments">
	<div class="modal__flexcontainer">
		<div class="modal_comment_container">
			<div class="modal__close"></div>
			<div class="event_comments">
				<div class="event_comments__header">
					<h2>コメント (201件)</h2>
				</div>
				<div class="event_comment_list">
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/don.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Don Flores</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">居酒屋メニューなので和系ならなんでも出てきます。外国の和食レストランみたい。でも有名なので味のわりには高いなとの印象です。キルビル的観光地として覚悟してご利用下さい。</p>
						</a>
					</div>
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/lucinda.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Lucinda Murray</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
						</a>
					</div>
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Thelma Pogi</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
						</a>
					</div>
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Thelma Pogi</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<main class="event_detail">
	<div class="container02 event_detail__container">
		<div class="main_detail">
			<section class="event_main_detail">
				<div class="event_cover" style="background-image: url('./assets/img/event/pic_01.png')">
					<div class="event_cat">デザイン・アート</div>
				</div>
				<h1 class="event_title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！- </h1>
				<div class="event_info">
					<span class="event_info__date">2017/11/1(水) ～ 2018/1/8(月)</span>
					<span class="event_info__location">森アーツセンターギャラリー</span>
					<span class="event_info__currency">3,000円 ～ 5,000円</span>
					<span class="event_info__users">無制限</span>
				</div>
				<div class="event_map_container sp">
					<div class="event_map">
						<div class="mapouter"><div class="gmap_canvas"><iframe width="640" height="120" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div></div>
					</div>
					<div class="event_address">東京都豊島区雑司が谷３丁目15-20</div>
				</div>
				<div class="event_owner sp">
					<div class="event_owner__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
					<div class="event_owner__details">
						<div class="event_owner__username">Thelma Pogi</div>
						<div class="event_owner__follow">フォローする</div>
						<div class="event_owner__mail"></div>
					</div>
				</div>
				<div class="event_description">
					<h2>イベント紹介</h2>
					<p>東京の観光に関するおでかけプランを集めたページです。<br>東京観光といえば、浅草やお台場、東京タワーやスカイツリーなどが有名ですが、他にもたくさんの観光名所があります。<br> 最新の流行だけでなく伝統文化に触れることのできる東京は刺激に満ちた街並みがたくさんあります。 <br>六本木や表参道、渋谷でショッピングしたり、神楽坂や吉祥寺、中目黒などでのんびり過ごすのも良いかもしれません。新宿で高層ビル群の夜景を眺めるのはいかがですか？ 国内だけでなく、海外からも多くの旅行者が訪れる東京。 <br>外国人の友達を案内したり、友達と東京グルメツアーをしたりなど、様々なシーンに合わせてたくさんある東京観光プランの中から自分にぴったりのものを探してみてください。おでかけプランは地図にも表示されるので観光マップとしても大活躍です。<br>気に入った観光スポットだけを集めて自分だけの観光マップをつくることもできます。</p>
				</div>
			</section>
			<section class="photo_slider_container">
				<h2>アルバム</h2>
				<div class="photo_slider">
					<div class="photo_preview_slider">
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_05.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_06.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_07.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_08.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_01.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_02.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_03.png');"></div>
						</div>
						<div modal-target="modal--slider">
							<div class="preview_slide" style="background-image: url('./assets/img/event/pic_04.png');"></div>
						</div>
					</div>
					<div class="photo_preview_slider__prev"></div>
					<div class="photo_preview_slider__next"></div>
				</div>
			</section>
			<section class="event_participants">
				<div class="event_participants__header">
					<h2>現在の参加者</h2>
					<div class="txt--blue">See All (18人)</div>
				</div>
				<ul class="participants">
					<li style="background-image: url('./assets/img/user/user_01.png"></li>
					<li style="background-image: url('./assets/img/user/user_02.png"></li>
					<li style="background-image: url('./assets/img/user/user_03.png"></li>
					<li style="background-image: url('./assets/img/user/user_04.png"></li>
					<li style="background-image: url('./assets/img/user/user_05.png"></li>
					<li style="background-image: url('./assets/img/user/user_06.png"></li>
					<li style="background-image: url('./assets/img/user/user_07.png"></li>
					<li style="background-image: url('./assets/img/user/user_08.png"></li>
					<li style="background-image: url('./assets/img/user/user_09.png"></li>
					<li style="background-image: url('./assets/img/user/user_10.png"></li>
					<li style="background-image: url('./assets/img/user/user_11.png"></li>
					<li style="background-image: url('./assets/img/user/user_12.png"></li>
					<li style="background-image: url('./assets/img/user/user_13.png"></li>
					<li style="background-image: url('./assets/img/user/user_14.png"></li>
					<li style="background-image: url('./assets/img/user/user_15.png"></li>
					<li style="background-image: url('./assets/img/user/user_16.png"></li>
					<li style="background-image: url('./assets/img/user/user_17.png"></li>
					<li style="background-image: url('./assets/img/user/user_18.png"></li>
				</ul>
			</section>
			<section class="event_comments">
				<div class="event_comments__header">
					<h2>コメント</h2>
					<div class="txt--blue" modal-target="modal--comments">全部のコメントを見る(201件)</div>
				</div>
				<div class="event_comment_list">
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/don.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Don Flores</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">居酒屋メニューなので和系ならなんでも出てきます。外国の和食レストランみたい。でも有名なので味のわりには高いなとの印象です。キルビル的観光地として覚悟してご利用下さい。</p>
						</a>
					</div>
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/lucinda.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Lucinda Murray</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
						</a>
					</div>
					<div class="comment_item">
						<div class="comment_item__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
						<a class="comment_item__details">
							<p class="comment_item__name">Thelma Pogi</p>
							<p class="comment_item__date">04/16 12:12</p>
							<p class="comment_item__msg">まさに上質のこじんまりとした権八というような雰囲気の古民家風ワイン和食ですので、外国人に喜ばれます。味はビブグルマンに選ばれたということからも定評ありますね。大人数なら権八へ。少人数ならこちら。大人数でもこちらを貸切という選択肢もあります。</p>
						</a>
					</div>
				</div>
				<div class="add_comment">
					<form>
						<div class="form_input">
							<textarea placeholder="コメントをかく..."></textarea>
						</div>
						<div class="form_input reply_btn">
							<label class="custom_checkbox">
								利用規約を同意する
							  	<input type="checkbox" checked="checked">
							  	<span class="checkmark"></span>
							</label>
							<input type="submit" name="" value="コメントする" class="btn btn--primary">
						</div>
					</form>
				</div>
			</section>
		</div>
		<div class="sub_detail pc">
			<div class="event_owner">
				<div class="event_owner__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
				<div class="event_owner__details">
					<div class="event_owner__username">Thelma Pogi</div>
					<div class="event_owner__follow">フォローする</div>
					<div class="event_owner__mail"></div>
				</div>
			</div>
			<div class="event_map">
				<div class="mapouter"><div class="gmap_canvas"><iframe width="640" height="640" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div></div>
			</div>
			<div class="event_address">東京都豊島区雑司が谷３丁目15-20</div>
			<div class="event_social">
				<ul>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_twitter.png">
						</a>
						twitter
					</li>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_fb.png">
						</a>
						facebook
					</li>
					<li>
						<a href="">
							<img src="./assets/img/common/ico_googleplus.png">
						</a>
						google+
					</li>
				</ul>
			</div>
			<button class="btn btn--primary_invert">参加する</button>
			<div class="event_interest"><span>興味のあるイベント</span></div>
		</div>
	</div>
	<div class="container02">
		<div class="event_slider_container">
			<div class="event_slider">
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_01.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_02.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">HOT！ほっとスイーツ2018</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_03.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">レアンドロ・エルリッヒ展：見ることのリアル</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_04.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">SNOW AQUARIUM by NAKED <br class="pc">ーCRYSTAL MAGICー</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_05.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_06.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_07.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_08.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
			</div>
			<div class="event_slider__prev"></div>
			<div class="event_slider__next"></div>
		</div>
	</div>
	<div class="sub_detail sp container02">
		<button class="btn btn--primary_invert">参加する</button>
		<div class="sub_detail_clickables">
			<div class="event_social">
				<a href="" class="twitter">
					<img src="./assets/img/common/ico_twitter_sm.png">
				</a>
				<a href="" class="fb">
					<img src="./assets/img/common/ico_fb_sm.png">
				</a>
				<a href="" class="google">
					<img src="./assets/img/common/ico_googleplus_sm.png">
				</a>
			</div>
			<div class="event_interest"><span>興味のあるイベント</span></div>
		</div>
	</div>
		
</main>

<?php include_once('includes/footer.php'); ?>