<?php 

	$page_title = "Edit Event";
	include_once('includes/header.php') 

?>

<main class="create_edit">
	<div class="container">
		<header class="form_header">
			<h1>イベント編集する</h1>
			<p class="pc">ダミーです。あなたが興味あるCIRCLEイベントを選択してください。</p>
			<p class="sp">あなたが興味あるCIRCLEイベントを選択してください。</p>
		</header>
	</div>
	<div class="create_edit__form default_form">
		<form>
			<div class="container">
				<div class="form_input">
					<label>タイトル</label>
					<input type="" class="full-width event_title" name="" placeholder="タイトル" value="MOVE 生きものになれる展 -動く図鑑の世界にとびこ">
				</div>
				<div class="form_input">
					<label>カテゴリー</label>
					<div class="custom_select">
					  <select>
					    <option value="1">音  楽</option>
					    <option value="2" selected="true">デザインアート</option>
					    <option value="3">劇・芝居</option>
					    <option value="4">フード</option>
					    <option value="5">ペット</option>
					    <option value="6">子供</option>
					  </select>
					</div>
				</div>
				<div class="form_input center_align">
					<label>日時</label>
					<input type="text" id="duration_from" class="datepicker" placeholder="2018/04/11 00:00">
					<div class="to"></div>
					<input type="text" id="duration_til" class="datepicker" placeholder="2018/04/11 00:00">
				</div>
				<div class="form_input">
					<label>場所</label>
					<input type="" class="full-width" name="" placeholder="東京都港区六本木6-10-1 六本木ヒルズ森タワー52階">
				</div>
				<div class="form_input">
					<label>参加人数</label>
					<input type="" name="" placeholder="参加人数">
				</div>
				<div class="form_input">
					<label>参加費</label>
					<input type="" name="" placeholder="3,000円〜５,000円">
				</div>
				<div class="form_input">
					<label>イベント写真</label>
					<div class="preview_gallery">
						<label class="upload_img"><input type="file" multiple name="" id="gallery-photo-add"></label>
						<div class="preview_slider_container">
							<div class="preview_slider">
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_05.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_06.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_07.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_01.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_02.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_03.png');"></div>
								</div>
								<div>
									<div class="preview_slide" style="background-image: url('./assets/img/event/pic_04.png');"></div>
								</div>
							</div>
							<div class="preview_slider__prev"></div>
							<div class="preview_slider__next"></div>
						</div>
					</div>
				</div>
				<div class="form_input">
					<label>イベント紹介</label>
					<textarea placeholder="" class="full-width">東京の観光に関するおでかけプランを集めたページです。東京観光といえば、浅草やお台場、東京タワーやスカイツリーなどが有名ですが、他にもたくさんの観光名所があります。 最新の流行だけでなく伝統文化に触れることのできる東京は刺激に満ちた街並みがたくさんあります。 六本木や表参道、渋谷でショッピングしたり、神楽坂や吉祥寺、中目黒などでのんびり過ごすのも良いかもしれません。新宿で高層ビル群の夜景を眺めるのはいかがですか？ 国内だけでなく、海外からも多くの旅行者が訪れる東京。 外国人の友達を案内したり、友達と東京グルメツアーをしたりなど、様々なシーンに合わせてたくさんある東京観光プランの中から自分にぴったりのものを探してみてください。おでかけプランは地図にも表示されるので観光マップとしても大活躍です。気に入った観光スポットだけを集めて自分だけの観光マップをつくることもできます。</textarea>
				</div>
				<div class="form_input">
					<label>公開設定</label>
					<label class="custom_radio">
						一般公開
						<input type="radio" name="rad" checked>
						<span class="checkmark"></span>
					</label>
					<label class="custom_radio">
						友達のみ
						<input type="radio" name="rad">
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="form_input submit_btn">
				<div class="container">
					<button class="btn btn--white"><span>キャンセル</span></button>
					<input class="btn btn--primary_invert" type="submit" value="登録する">
				</div>
			</div>
		</form>
	</div>
</main>

<?php include_once('includes/footer.php') ?>