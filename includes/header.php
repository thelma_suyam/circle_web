<!DOCTYPE html>
<html class="no-js" lang="ja">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $page_title; ?> | Circle</title>
		<!-- META INFO -->
		<!-- <meta name="description" content="">
		<meta name="keywords" content="">
		<meta property="og:title" content="トップ">
		<meta property="og:type" content="website">
		<meta property="og:description" content="">
		<meta property="og:url" content="./">
		<meta property="og:image" content="./assets/img/og.png">-->
		<!-- FAVICON -->
		<link rel="shortcut icon" href="./assets/img/favicon.ico">
		<link rel="icon" href="./assets/img/favicon.ico">
		<!-- FONTS -->
		<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:300,400" rel="stylesheet">
		<!-- CSS -->
		<!-- <link rel="stylesheet" href="./assets/css/swiper.min.css"> -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="./assets/css/normalize.css">
		<link rel="stylesheet" href="./assets/css/vendor/slick.css">
		<link rel="stylesheet" href="./assets/css/vendor/slick-theme.css">
		<link rel="stylesheet" href="./assets/css/main.css">
		<link rel="stylesheet" href="./assets/css/jquery-ui-datepicker-custom.css">
		<link rel="stylesheet" href="./assets/css/jquery-ui-datepicker-custom.theme.css">
		<!-- SCRIPT -->
		<script src="./assets/js/vendor/modernizr-2.8.3.min.js"></script>
		<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
		<script>
				(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
				function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
				e=o.createElement(i);r=o.getElementsByTagName(i)[0];
				e.src='//www.google-analytics.com/analytics.js';
				r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
				ga('create','UA-XXXXX-X','auto');ga('send','pageview');
		</script> -->
	</head>


	<body id="top">
		<header>
			<div class="pc">
				<div class="main_header">
					<div class="main_header__col">
						<a href="./" class="logo">
							<h1>
								<img src="./assets/img/common/logo.png" alt="">
							</h1>
						</a>
						<a class="btn btn--primary" href="create-event.php">
							<span>イベント作成</span>
						</a>
					</div>
					<div class="main_header__col">
						<div class="user_notif">
							<div class="notif on" data-target="notifications">
								<div class="notif__content">
								</div>
							</div>
							<div class="user_notif__list" data-id="notifications">
								<ul class="notif_tab">
									<li>
										<a header-panel="notifications" class="active"><span>お知らせ</span></a>
									</li>
									<li>
										<a header-panel="messages"><span>受信トレイ</span></a>
									</li>
								</ul>
								<div class="visible notif_panel" header-panel-id="notifications" >
									<div class="notif_list">
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/clifford.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">Clifford Carr</p>
												<p class="notif_item__msg">Live Androidを参加しました。</p>
												<p class="notif_item__date">今</p>
											</a>
										</div>
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/michael.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">Michael</p>
												<p class="notif_item__msg">ユッキーをフォローしました。</p>
												<p class="notif_item__date">3時間前</p>
											</a>
										</div>
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/jlo.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">jlo</p>
												<p class="notif_item__msg">あなたのLive iOSのイベントに参加者jloさんが追加されました。</p>
												<p class="notif_item__date">2018/04/12</p>
											</a>
										</div>
									</div>
									<a href="notif-message.php" class="see_all">See All</a>
								</div>
								<div class="notif_panel" header-panel-id="messages" hidden>
									<div class="notif_list">
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/thelma.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">Thelma Pogi</p>
												<p class="notif_item__msg">Lorem ipsum dolor sit amet</p>
												<p class="notif_item__date">2018/09/12</p>
											</a>
										</div>
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/rosetta.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">Rosetta</p>
												<p class="notif_item__msg">ユッキーをフォローしました。</p>
												<p class="notif_item__date">3時間前</p>
											</a>
										</div>
										<div class="notif_item">
											<div class="notif_item__img" style="background-image: url('./assets/img/user/eula.png')"></div>
											<a class="notif_item__details">
												<p class="notif_item__name">Eula</p>
												<p class="notif_item__msg">のイベントに参加者jloさんが追加されました。</p>
												<p class="notif_item__date">2018/04/12</p>
											</a>
										</div>
									</div>
									<a href="notif-message.php" class="see_all">See All</a>
								</div>
							</div>
						</div>
						<div class="user_nav">
							<div class="user_icon" data-target="user_nav_list" style="background-image: url('./assets/img/user/pic_01.png')">
							</div>
							<div class="user_nav__list" data-id="user_nav_list">
								<ul>
									<li>
										<a href="profile-schedule.php" class="active">
											<span>参加予定イベント</span>
										</a>
									</li>
									<li>
										<a href="profile-schedule.php">
											<span>参加したイベント</span>
										</a>
									</li>
									<li>
										<a href="profile-create.php">
											<span>作成イベント</span>
										</a>
									</li>
									<li>
										<a href="">
											<span>興味のあるイベント</span>
										</a>
									</li>
									<li>
										<a href="profile-category.php"  class="bordered">
											<span>カテゴリー</span>
										</a>
									</li>
									<li>
										<a href="add-friends.php" class="bordered">
											<span>フォロー</span>
										</a>
									</li>
									<li>
										<a href="followers.php" class="bordered">
											<span>友達追加</span>
										</a>
									</li>
									<li>
										<a href="">
											<span>アカウント設定</span>
										</a>
									</li>
									<li>
										<a href="">
											<span>ログアウト</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div> <!-- .main_header PC -->
			</div>
			<div class="sp">
				<div class="main_header">
					<div class="main_header__col" data-target="sp_menu">
						<div class="btn--menu_toggle">
							<div></div>
							<div></div>
							<div></div>
						</div>
					</div>
					<div class="main_header__col">
						<a href="./" class="logo">
							<h1>
								<img src="./assets/img/common/logo.png" alt="">
							</h1>
						</a>
					</div>
					<div class="main_header__menu" data-id="sp_menu">
						<div class="sp_menu_content">
							<ul>
								<li><a href="" class="search">検索</a></li>
								<li><a href="profile-schedule.php" class="user">マイページ</a></li>
								<li><a href="notif-message.php" class="notif">お知らせ</a></li>
							</ul>
							<button class="btn btn--primary">イベント作成</button>
							<button class="btn btn--white">お問合せはこちら</button>
							<div class="social_btns">
								<a href=""><img src="./assets/img/common/ico_fb_sp.png" alt=""></a>
								<a href=""><img src="./assets/img/common/ico_twitter_sp.png" alt=""></a>
							</div>
							<p>© comMu:de 2011 inc.</p>
						</div>
					</div>
				</div> <!-- .main_header SP -->
			</div>
		</header>