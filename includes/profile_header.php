<header class="profile_header container">
	<div class="profile_header__col">
		<div class="profile_header__img" style="background-image: url('./assets/img/user/johnny.png')">
		</div>
	</div>
	<div class="profile_header__col">
		<h2 class="profile_header__username">Jayden Moran</h2>
		<p class="profile_header__bio">音楽・演劇が好きで常にイベントに参加しております！</p>
		<div class="profile_header__info">
			<div class="profile_info">
				<div class="profile_info__col"><a href="add-friends.php">友達を追加</a></div>
				<div class="profile_info__col"><a href="">アカウント設定</a></div>
			</div>
			<div class="follow_info">
				<div class="follow_info__col">
					フォロー<span>52</span>
				</div>
				<div class="follow_info__col">
					<a href="followers.php">フォロワー<span>203</span></a>
				</div>
			</div>
		</div>
	</div>
</header>


<div class="modal modal_profile">
	<div class="modal__inner">
		<a href="#" class="ico__close"><img src="./assets/img/common/ico_close.png" alt=""></a>
		<div class="modal_content">
			<form>
				<div class="modal_profile__img">
					<img src="./assets/img/user/johnny.png" alt="">
				</div>
				<div class="form_input">
					<label class="change_img"><input type="file" name="">画像を変更する</label>
				</div>
				<div class="form_input">
					<input type="text" name="" placeholder="Jayden Moran">
				</div>
				<div class="form_input">
					<textarea placeholder="音楽・演劇が好きで常にイベントに参加しております！"></textarea>
				</div>
				<div class="form_input">
					<input type="submit" name="" value="確定" class="btn btn--primary">
				</div>
			</form>
		</div>
	</div>
</div>