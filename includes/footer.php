
		<div class="modal" id="login__modal" hidden>
			<div class="modal__inner">
				<a href="#" class="ico__close"><img src="./assets/img/common/ico_close.png" alt=""></a>
				<figure><img src="./assets/img/common/logo_modal.png" alt=""></figure>
				<div class="form__container">
					<input type="text" placeholder="メールアドレス">
					<input type="text" placeholder="パスワード">
					<input type="submit" value="ログイン">
				</div>
				<img src="./assets/img/common/pic_or.png" alt="" class="or--img">
				<ul class="social__container">
					<li><a href="#" class=""><img src="./assets/img/common/ico_fb.png" alt=""></a></li>
					<li><a href="#" class=""><img src="./assets/img/common/ico_twitter.png" alt=""></a></li>
					<li><a href="#" class=""><img src="./assets/img/common/ico_googleplus.png" alt=""></a></li>
				</ul>
				<a href="#" class="a--register"><span>新規登録</span>はこちら</a>
			</div>
		</div>

		<div class="modal" id="register__modal" hidden>
			<div class="modal__inner">
				<a href="#" class="ico__close"><img src="./assets/img/common/ico_close.png" alt=""></a>
				<figure><img src="./assets/img/common/logo_modal.png" alt=""></figure>
				<div class="form__container">
					<input type="text" placeholder="ユーザー名">
					<input type="text" placeholder="メールアドレス">
					<input type="submit" value="登 録">
				</div>
				<img src="./assets/img/common/pic_or.png" alt="" class="or--img">
				<ul class="social__container">
					<li><a href="#" class=""><img src="./assets/img/common/ico_fb.png" alt=""></a></li>
					<li><a href="#" class=""><img src="./assets/img/common/ico_twitter.png" alt=""></a></li>
					<li><a href="#" class=""><img src="./assets/img/common/ico_googleplus.png" alt=""></a></li>
				</ul>
				<a href="#" class="a--register"><span>ログイン</span>はこちら</a>
			</div>
		</div>

		<footer>
			<div class="footer__container">
				<img src="./assets/img/common/logo_footer.png" alt="" class="logo__footer pc">
				<img src="./assets/img/common/logo_footer_sp.png" alt="" class="logo__footer sp">
				<div class="download__link">
					<a href="#" class="btn__appstore"></a>
					<a href="#" class="btn__googleplay"></a>
				</div>
				<div class="company__list">
					<div class="company__item">
						<img src="./assets/img/common/logo_footer_commude.png" alt="">
					</div>
					<div class="company__item">
						<img src="./assets/img/common/logo_footer_circle.png" alt="">
					</div>
				</div>
				<small>© comMu:de 2011 inc.</small>
			</div>
		</footer>

		<script src="./assets/js/vendor/jquery-3.1.1.min.js"></script>
		<script src="./assets/js/vendor/jquery.inview.js"></script>
		<script src="./assets/js/vendor/jquery.matchHeight-min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="./assets/js/vendor/swiper.min.js"></script>
		<script src="./assets/js/vendor/slick.js"></script>
		<script src="./assets/js/vendor/datepicker-ja.js"></script>
		<script src="./assets/js/main.js"></script>
	</body>
</html>
