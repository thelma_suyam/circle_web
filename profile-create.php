<?php 
	
	$page_title = "Add Event";
	include_once('includes/header.php');
	include_once('includes/profile_header.php');

?>

<nav class="profile_nav container">
	<ul>
		<li>
			<a href="profile-schedule">参加予定<br class="sp">イベント</a>
		</li>
		<li>
			<a href="">参加した<br class="sp">イベント</a>
		</li>
		<li>
			<a href="profile-create" class="active">作成<br class="sp">イベント</a>
		</li>
		<li>
			<a href="">興味のある<br class="sp">イベント</a>
		</li>
		<li>
			<a href="profile-category">カテゴリー</a>
		</li>
	</ul>
</nav>

<main class="profile">
	<div class="container">
		<!-- create event -->
		<div class="create_event profile_panel" id="create_event">
			<div class="event_list">
				<div class="event_item">
					<div class="create_event__box">
						<img src="./assets/img/common/ico_plus_red.png" alt="">
						<span>イベント作成</span>
					</div>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_02.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">HOT！ほっとスイーツ2018</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_03.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">レアンドロ・エルリッヒ展：見ることのリアル</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
				<div class="event_item">
					<a href="event-detail.php">
						<div class="event_item__img" style="background-image: url('./assets/img/event/pic_04.png')"></div>
						<div class="event_item__info">
							<h3 class="event_item__title">SNOW AQUARIUM by NAKED <br class="pc">ーCRYSTAL MAGICー</h3>
							<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
							<span class="event_item__location">森アーツセンターギャラリー</span>
							<span class="event_item__currency">3,000円 ～ 5,000円</span>
						</div>
					</a>
				</div>
			</div>
		</div><!-- /create event -->
	</div>
</main>

<?php include_once('includes/footer.php') ?>