<?php 
	
	$page_title = "Map";
	include_once('includes/header.php') ?>

<main class="map">
	<div class="map__col map_event pc">
		<div class="previous">
			<div class="form_input">
				<input type="text" class="input--search" name="" placeholder="豊島区">
			</div>
		</div>
		<div class="event_list">
			<div class="event_item">
				<a href="event-detail.php">
					<div class="event_item__img" style="background-image: url('./assets/img/event/pic_02.png')"></div>
					<div class="event_item__info">
						<h3 class="event_item__title">HOT！ほっとスイーツ2018</h3>
						<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
						<span class="event_item__location">森アーツセンターギャラリー</span>
						<span class="event_item__currency">3,000円 ～ 5,000円</span>
					</div>
				</a>
			</div>
			<div class="event_item">
				<a href="event-detail.php">
					<div class="event_item__img" style="background-image: url('./assets/img/event/pic_03.png')"></div>
					<div class="event_item__info">
						<h3 class="event_item__title">レアンドロ・エルリッヒ展：見ることのリアル</h3>
						<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
						<span class="event_item__location">森アーツセンターギャラリー</span>
						<span class="event_item__currency">3,000円 ～ 5,000円</span>
					</div>
				</a>
			</div>
			<div class="event_item">
				<a href="event-detail.php">
					<div class="event_item__img" style="background-image: url('./assets/img/event/pic_04.png')"></div>
					<div class="event_item__info">
						<h3 class="event_item__title">SNOW AQUARIUM by NAKED <br class="pc">ーCRYSTAL MAGICー</h3>
						<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
						<span class="event_item__location">森アーツセンターギャラリー</span>
						<span class="event_item__currency">3,000円 ～ 5,000円</span>
					</div>
				</a>
			</div>
			<div class="event_item">
				<a href="event-detail.php">
					<div class="event_item__img" style="background-image: url('./assets/img/event/pic_05.png')"></div>
					<div class="event_item__info">
						<h3 class="event_item__title">MOVE 生きものになれる展 -動く図鑑の世界にとびこもう！-</h3>
						<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
						<span class="event_item__location">森アーツセンターギャラリー</span>
						<span class="event_item__currency">3,000円 ～ 5,000円</span>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="map__col map_container">
		<div class="previous sp">
			<div class="form_input">
				<input type="text" class="input--search" name="" placeholder="豊島区">
			</div>
		</div>
		<div class="event_list sp">
			<div class="event_item">
				<div class="event_item__info">
					<h3 class="event_item__title">HOT！ほっとスイーツ2018</h3>
					<span class="event_item__date">2017/11/1(水) ～ 2018/1/8(月)</span>
					<span class="event_item__location">森アーツセンターギャラリー</span>
					<span class="event_item__currency">3,000円 ～ 5,000円</span>
				</div>
			</div>
		</div>
		<div class="mapouter"><div class="gmap_canvas"><iframe width="640" height="120" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div></div>
	</div>
</main>

<?php include_once('includes/footer.php') ?>